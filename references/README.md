split the icon into SVG format
- https://github.com/encharm/Font-Awesome-SVG-PNG

Free vector onlines
- https://icomoon.io/#icons-icomoon
- http://www.flaticon.com/packs/miscellaneous-elements

Font-Awesome
- https://code.tutsplus.com/tutorials/how-to-use-fontawesome-in-an-android-app--cms-24167

Custom Iconic Library for Android
- http://blog.joanzapata.com/iconify-just-got-a-lot-better/
- https://github.com/JoanZapata/android-iconify 
- http://www.webalys.com/nova/